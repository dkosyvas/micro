;msb[3] msb[2] msb[1] msb[0]  lsb[3] lsb[2] lsb[1] lsb[0]

;if lsb[0]=1 then led moves in sequence A[1 2 3 4 5 6 7 8 7 6 4 3 2 1 2 3 4 5 6 7 8 7 .. ]
;if lsb[0]=0 then led moves circular    B[1 2 3 4 5 6 7 8 1 2 3 4 5 6 7 8 ...]
;if lsb[1]=1 led in position  lsb[0] lights and doesnt move until lsb[1]=0

START:
        LXI H,2000H ; store in H-L registes (M) address of input from dip switches 
        LXI	B,01F4H ;delay 0.5 s
        MVI A,FEH   ;  lsb[0]=11111110 negative logic
        STA 3000H   ; light led in lsb[0] position
        CALL DELB
        JMP LEFT1
        
FX_L:   RLC         ;shift right because we need the lsb[0] in accumulator and not the msb[3] that we have after last rotate right
        
LEFT1:
        MOV D,M     ; copy dip switch input to C 
        DCR D       ; if lsb[0]==1 follow A sequence  else if lsb[0]==0 follow B (circular)
        JNZ CIRC
        MOV E,A     ;save accumulator in B register
        MOV A,M
        ANI 02H     ;check for lsb[1]
        CPI 00H     
        JNZ REM     ;if lsb[1] is on go to REM light lsb[0] and wait 
        MOV A,E     ;recover accumulator
        RLC         ;rotate left accumulator if a=0111 1111 negative logic then in next left rotate CY=0 and a=1111 1110 
        JNC FX_R
        STA 3000H   ;light led 
        CALL DELB
        JMP LEFT1 
        

    
        
        
FX_R:   RRC         ;shift left because we need the msb[3] in accumulator and not the lsb[0] that we have after last rotate left

RIGHT1:
        MOV D,M     ; copy dip switch input to C 
        DCR D       ; if lsb[0]==1 follow A sequence  else if lsb[0]==0 follow B (circular)
        JNZ CIRC
        MOV E,A     ;save accumulator in E register
        MOV A,M
        ANI 02H     ;check for lsb[1]
        CPI 00H     
        MOV A,E     ;save A
	 JNZ REM     ;if lsb[1] is on go to REM light lsb[0] and wait 
        RRC         ;rotate right  accumulator if a=1111 1110 negative logic then in next left rotate CY=0 and a=0111 1111  
        JNC FX_L
        STA 3000H   ;light led 
        CALL DELB
        JMP RIGHT1 
        
    
    

    
CIRC:   MOV D,M               ;circular mode
        DCR D                 ; check again if dip switch [0]  is on 
        JZ LEFT1
        MOV E,A
        MOV A,M
        ANI 02H               ; check if lsb[1]=1 
        CPI 00H
        MOV A,E
	 JNZ REM
        RLC                   ; Rotate left accumulator to move led to left (Circular from left to right)
        STA 3000H             ; light led 
        CALL DELB
        JMP CIRC
        
REM:    MOV L,A
        MVI A,FEH              ;lsb[0] led is on until second dip switch lsb[1] changes from on to off
        STA 3000H             ; light led 
        CALL DELB
        MOV A,M             
        ANI 02H               ;check if lsb[1]=1
        CPI 00H               
        MOV A,L
        JZ LEFT1              ;IF Z=0 lsb[1]=0 whether lsb[0]=1 or lsb[0]=0 we dont care here
        JMP REM
        
        
END
        