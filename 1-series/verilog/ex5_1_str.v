
module ex5_1 (A, B, C, D,F1);
    output F1;
    input A, B, C,D;
    wire w1,w2,w3,w4,d_not,c_not;
    and G1 (w1, C, D); // Optional gate instance name (G1)
    or  G2 (w2,w1,B);
    and G3 (w3,w2,A);
    not invd (d_not,D);
    not invc (c_not,C);
    and G4 (w4,B,d_not,c_not);
    or  G5 (F1,w3,w4);
endmodule