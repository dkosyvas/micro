module ex5_1_dt (A, B, C, D,F1);
    output  F1;
    input A, B, C, D;
    assign F1 = (A&(C&D |B)) | (B & ~C &~D) ;
endmodule