

#include <avr/io.h>
unsigned char a, b, c, notb,notc, d, f0, f1;

int main(void){
	DDRB = 0x00;	//portB input
	DDRA = 0xff;	//portA output 
	
	while(1){
		a = PINB & 0x01;
		b = PINB & 0x02;
		b = b>>1;
		c = PINB & 0x04;
		c = c>>2;
		d = PINB & 0x08;
		d = d>>3;


		f1 = (a | c) & (b | d);
		f1 = f1<<1;		

		notc = c^0x01;	//complementary in bits is achieved with XOR		
        notb = b^0x01;  //complementary of b 
		f0 = ((a & notb ) | (b & notc & d));
		f0 = f0^0x01;
		
		f0 = f0 + f1;
		PORTA = f0;
	}

	return 0;	
}
