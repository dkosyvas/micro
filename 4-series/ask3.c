/*
Ζήτημα 2.3 Να γραφτεί πρόγραμμα σε C για τον προγραμματισμό AVR Atmega16 και να γίνει προσομοίωση στο
Atmel Studio 7 το οποίο αρχικά να ανάβει το led0 που είναι συνδεδεμένο στο bit0 της θύρας εξόδου PORTΒ. Στην
συνέχεια με το πάτημα των διακοπτών (Push-buttons) SW0-3 που υποθέτουμε ότι είναι συνδεδεμένα στα αντίστοιχα
bit της θύρας εισόδου PORTΑ να συμβαίνουν τα εξής:
• SW0 ολίσθηση-περιστροφή του led μια θέση δεξιά (κυκλικά).
• SW1 ολίσθηση-περιστροφή του led μια θέση αριστερά (κυκλικά).
• SW2 μετακίνηση του αναμμένου led στην αρχική του θέση (LSB-led0).
• SW3 μετακίνηση του αναμμένου led στην θέση ΜSB (led7).
Όλες οι αλλαγές να γίνονται όταν αφήνεται κάθε Push-button SWx (bitx PORTA). Υποθέτουμε ότι τα push buttons
δεν θα πατιούνται ταυτόχρονα.
*/

#include <avr/io.h>
unsigned char x;
 
int main(void){
	DDRB = 0xff;    //portB output
	DDRA = 0x00;    //portA input 
     
	x = 1;
	while(1){
		if((PINA & 0x01) == 1){
			//first push-button is pushed
			while((PINA & 0x01) == 1){} //wait for push-button release
			if(x == 0x01){	// IF 0000 0001 THEN RESET X to  1000 0000
				x = 0x80;
			}
			else{
				x = x >> 1; //right rotate by 1 bit
			}
		}
		if((PINA & 0x02) == 2){			
			//second push-button is pushed
			while((PINA & 0x02) == 2){} //wait for push-button release
			if(x == 0x80){
				x = 0x01;   //reset value of x
			}
			else{
				x = x<<1;   // rotate left by 1 bit 
			}
		}
		if((PINA & 0x04) == 4){
			//third push-button is pushed
			while((PINA & 0x04) == 4){} //wait for push-button release
			x = 0x01;  // μετακίνηση του αναμμένου led στην αρχική του θέση  lsb-led0              
		}
		if((PINA & 0x08) == 8){
			//fourth push-button is pushed
			while((PINA & 0x08) == 8){} //wait for push-button release
			x = 0x80; //μετακίνηση του αναμμένου led στην θέση ΜSB (led7).
		}
		PORTB = x;
	}
	return 0;   
}

