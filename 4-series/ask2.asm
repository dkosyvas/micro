 
;F0 = (AB' + BC'D)'
;F1= (A+C)&(B+D)

.INCLUDE "m16def.inc"

.def input=r18
.def temp=r19
.def regForXor=r17
.def A=r20
.def B=r21
.def C=r22
.def D=r23
.def F0=r24
.def F1=r25

	ldi regForXor,1
main:	
	clr temp
	out DDRB,temp		;portB for input
	ser temp
	out DDRA,temp		;portA for output

	in input,PINB		;B
	mov A,input
	andi A,0x01		;make A in format 0000000x

	mov B,input		;B
	andi B,0x02
	ror B			;make B in format 0000000x

	mov C,input		;C
	andi C,0x04
	ror C
	ror C			;make C in format 0000000x

	mov D,input		;D
	andi D,0x08
	ror D
	ror D
	ror D			;make D in format 0000000x

	mov F0,A		;F0
	mov temp,B
	eor temp,regForXor;	;not in bits is achieved with XOR
	and F0,temp
	mov temp,C
	eor temp,regForXor
	and temp,B
	and	temp,D
	or	F0,temp
	and temp,D
	or F0,temp
	eor F0,regForXor

	mov F1,A		;F1
	or F1,B
	mov temp,C
	or temp,D
	and F1,temp
	
	clc
	rol F1
	add F0,F1
	out PORTA,F0		;result in portA

	rjmp main
end:
