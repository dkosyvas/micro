;This program has the input in portC and the output in portB.
;It implements a rotating led that starts from LSB of portB and rotates
;left. When it goes to the MSB it starts rotating right and so on, continuously. 
;When I press PC2 it should stop and when I release it it should 
;continue from where it has stopped.


.INCLUDE "m16def.inc" 

.def counter=r20 
.def temp=r21
.def lightBit=r22
.def inputC=r23

.org 0



main:
	clr temp			;reset temp
	out DDRC,temp			;portC for input
	ser temp			;set temp 
	out DDRB,temp			;portB for output
	
	ldi lightBit,0x01		
	out PORTB,lightBit		;flash the LSB
	ldi counter,7			;set counter value 7

left:
	in inputC,PINC			;read input
					
	andi inputC,4		;mask  wanted bit PINC[2] case other bits of PINC are on 
	subi inputC,4		
	breq left			;if input 0x01 stay on loop and do nothing
					;if not exit and do the following
	
	clc				
	rol lightBit			
	out PORTB,lightBit		;flash the next led to the left
	
	
	dec counter			;counter--
	cpi counter,0			;if counter == 0 I arrived the MSB so I go right
	breq right
	rjmp left			;if counter != 0 go left again

right:
	in inputC,PINC			
	andi inputC,4
	subi inputC,4
	breq right
	
	clc				 
	ror lightBit
	out PORTB,lightBit		;flash the next led to the right
	
	inc counter			;counter++
	cpi counter,7			;if counter == 7 I arrived the LSB so I go left
	breq left
	rjmp right			;if counter != 7 go right again

	

end:
