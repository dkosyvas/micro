;this program stores numbers 254,253 ... 0,255 in continuous memory places starting at
;memory table.
;then it finds and prints the average of the even numbers  and
;the maximum and minumum and then finishes .

;this program is the second exercise of the fifth set from 2020.

;------macros------------ 
read macro
    mov ah,8
    int 21h
endm

print macro char
    push ax
    push dx
    mov dl,char
    mov ah,2
    int 21h
    pop dx
    pop ax
endm

print_string macro string
    push ax
    push dx
    mov dx,offset string
    mov ah,9
    int 21h
    pop dx
    pop ax
endm

exit macro
	mov ax,4c00h
	int 21h
endm

new_line macro
	push dx
	push ax
	mov dx,13
	mov ah,2
	int 21h  
	mov dx,10
	mov ah,2
	int 21h
	pop ax
	pop dx
endm
 
;------data segment------------ 
    data_seg segment                        
    header db "exercise 2$"
    m1 db "Max=$"  
    m2 db "Min=$"
    m3 db "Avg=$" 
    table db 256 dup(?) 
    min db ?
    max db ?
    data_seg ends
;------code segment------------
    code_seg segment
    assume cs:code_seg,ds:data_seg
    
    ;----main------------------
main proc far
    mov ax,data_seg
    mov ds,ax
        
    print_string header 
    new_line
        
    mov al,254				;first number to be stored
    mov di,0   				;initialize index
store_array:            
    mov [table + di],al     		;store numbers in table[256]   
    dec al				;next number to be stored
    inc di				;i++
    cmp al,0 
    je  last				;if all number are stored exit the loop		
    jne store_array

last:
 
    mov [table + 255 ],255                  ;store  in table
	
    mov di,0                            ;adding even number to get the average 
    mov ah,0     
continue_add: 
    mov al,[table + di] 		;load number
    add dx,ax 				;add number
    add di,2				;load only even numbers  
    cmp di,257
    jl continue_add       	        ;if di gets > 257 exit
    
    mov ax,dx
    mov bh,0
    mov bl,128
    div bl				;divide sum with 128 to find average
    
    
    mov ah,0   				;is needed for procedure 'print_dec'     
    print_string m3 
    call print_dec
    new_line   
    
    mov di,0xffff
    mov min,0
    mov  max,0
find_min_max: 
;if min < current number then current number isn't the min
;but it may be the max
    inc di
    mov al,[table + di]
    cmp min,al				
    jna skip
    mov min,al				;if current number < min --> min = current number
    jmp min_max_found     
skip:
    cmp max,al
    ja find_min_max
    mov max,al				;if current number > max --> max = current number
    cmp di,257   
    jne find_min_max 
 
min_max_found: 
;when min and max are found print them
    mov ah,0
    mov al,min
    print_string m2 
    call print_dec 
    new_line
    mov ah,0
    mov al,max
    print_string m1 
    call print_dec 
    new_line   
          
endp
exit

;----------proc----------------
print_dec proc near
;input: number in bl 
;output: print the decimal number
    mov bl,10 
    mov cx,1 				;decades counter
loop_10: 
    div bl				;divide number with 10
    push ax                		;save units  
    cmp al,0 				;if quotient zero i have splitted 
    je print_digits_10      		;the whole number into dec digits          
    inc cx				;increase number of decades
    mov ah,0   
    jmp loop_10				;if quotient is not zero i have to divide again
print_digits_10:
    pop dx				;pop dec digit to be printed
    mov dl,dh
    mov dh,0				;dx = 00000000xxxxxxxx (ascii of number to be printed)
    add dx,30h				;make ascii code
    mov ah,2
    int 21h				;print
    loop print_digits_10       
    ret
endp print_dec      

code_seg ends

end main
