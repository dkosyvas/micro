;calculator that finds the sum and subtraction of 2 two-digit hexadecimals and displays the result in decimal form 
;example input usage 
;input: 2F+3A=..result 
;input: 2F-3A=..result 
;sumbol '+' and '-' can be given by the user for addition or subtraction and when '=' is pressed
;the result of the computation is displayed as output in decimal form. 
;The program stops running when 'Q'  or 'q' is pressed by the user .

;-------MACROS--------

read macro
    mov ah,8
    int 21h
endm

print macro char
    push ax
    push dx
    mov dl,char
    mov ah,2
    int 21h
    pop dx
    pop ax
endm

print_string macro string
    push ax
    push dx
    mov dx,offset string
    mov ah,9
    int 21h
    pop dx
    pop ax
endm

exit macro
	mov ax,4c00h
	int 21h
endm

new_line macro
	push dx
	push ax
	mov dx,13
	mov ah,2
	int 21h  
	mov dx,10
	mov ah,2
	int 21h
	pop ax
	pop dx
endm


data segment
    num dw 0000 ;arxikopoihsh se 0
    new_line db 0ah,0dh,'$'
    calc db 'Calculator: $' 
    X_EQUALS DB "X=$" 
    Y_EQUALS DB "Y=$" 
data ends

code segment 'code'
assume cs:code, ds:data, ss:stack 


main proc far

mov ax,data
mov ds,ax
mov ss,ax


print_string calc
new_line 
start:
    mov cx,'0' ;arxikopoihsh tou kataxwrhth pou tha yparxei to noumero
read_hex:
    mov bx,0
    mov cl,0
reading:
    call read_ascii_number
    cmp al,'='
    je reading
    cmp al,'+'
    je adding
    cmp al,'-'
    je subtracting
    add cl,01h
    cmp cl,03h
    jge reading 
    print al
continue:    
    cmp al,'A'
    jge next_if_char     ; ascii code of A > ascii code of decimal numbers

next: ;decimal numbers
    sub al,30h ;30h
               ;so that we get the number value
    mov ah,0
    push ax
    mov ax,16
    mul bx ;in the end bx will have the number we have given
    mov bx,ax
    pop ax
    add bx,ax ;bx=bx+ax
    jmp reading 
    
next_if_char: 
    sub al,55
    mov ah,0
    push ax   
    mov ax,16
    mul bx ;in the end bx will have the number we have given
    mov bx,ax
    pop ax
    add bx,ax ;bx=bx+ax 
    jmp reading 

adding:
;first number is read, stored at bx
    cmp cl,0
    je reading
    print al 
    push bx ;tha kanoume pop meta gia na paroume ton proto arithmo
    mov cx,'0'
    mov bx,0
    mov cl,0

read2:
    call read_ascii_number
    cmp al,'=' ;if '=' is pressed then print the result
    je out1
    add cl,01h
    cmp cl,03h ;if 4th time i dont care what you type 
    jge read2
    
    print al ;print what we have press
continue2:    
    cmp al,'A'
    jge next_if_char2 
    
;here we calculate the decimal number that is pressed
next2:
    sub al,30h ;30h
    ;so that we get the number value
    mov ah,0
    push ax
    mov ax,16
    mul bx ;in the end bx will have the second number we have given
    mov bx,ax
    pop ax
    add bx,ax ;bx=bx+ax    
    jmp read2 
next_if_char2: 
    sub al,55
    mov ah,0
    push ax   
    mov ax,16
    mul bx ;in the end bx will have the second number we have given
    mov bx,ax
    pop ax
    add bx,ax ;bx=bx+ax
    jmp read2

out1:
    cmp cl,0
    je read2
    print '='
    mov ax,bx       ; AX<-second number
    pop bx          ; BX<-first number
    add bx,ax  
    jmp hex_output
subtracting:
;first number is read, stored at bx
    cmp cl,0
    je reading
    print al
    push bx ;tha kanoume pop meta gia na paroume ton proto arithmo
    mov cx,'0'
    mov bx,0
    mov cl,0

read3:
    call read_ascii_number
    cmp al,'=' ;if '=' is pressed then print the result
    je out2
    add cl,01h
    cmp cl,04h ;if 4th time i dont care what you type
    jge read3
    print al ;print what we have press

continue3:    
    cmp al,'A'
    jge next_if_char3
;here we calculate the decimal number that is pressed
next3:
    sub al,30h ;30h
    ;so that we get the number value
    mov ah,0
    push ax
    mov ax,16
    mul bx ;in the end bx will have the numver we have given
    mov bx,ax
    pop ax
    add bx,ax ;bx=bx+ax
    jmp read3    
next_if_char3: 
    sub al,55
    mov ah,0
    push ax   
    mov ax,16
    mul bx ;in the end bx will have the second number we have given
    mov bx,ax
    pop ax
    add bx,ax ;bx=bx+ax
    jmp read3  
  
out2:
    cmp cl,0
    je read3
    print al
    mov ax,bx
    pop bx
    cmp bx,ax ;An bx<ax tote arnitiko apotelesma
    jl arnitikos
    sub bx,ax
    jmp subt
arnitikos:
    
    ;An einai arnitikos ektyponoume to kai
    ;to kratame kai gia to allo output
    mov cx,'-'
    sub ax,bx
    mov bx,ax
subt:
    jmp hex_output
dec_output:
    
    cmp cx,'-'
    je arnitikos2
go_on:
    mov ax,bx
    call display
    jmp the_end
hex_output:
    push ax
    push bx
    push dx
    push cx
    mov ax,bx
    call dec_to_hex
    pop cx
    pop dx
    pop bx
    pop ax
    jmp dec_output
arnitikos2:
    print '-'
    jmp go_on

the_end:
    NEW_LINE
    jmp start

ending:
    exit
main endp

read_ascii_number proc near ;anagnwsh eisodou

read_valid_input:
    read ;read char
    cmp al,'Q'
    je exiting ;if al=='q' or al=='q' quit
    cmp al,'q'
    je exiting
    cmp al,'+'
    je valid
    cmp al,'-'
    je valid
    cmp al,'='
    je valid
    cmp al,'A'
    je valid
    cmp al,'0' ;if al<'0' ignore
    jl read_valid_input
    cmp al,'F'
    jle valid
    cmp al,'F' ;if al>'9' ignore
    jg read_valid_input

exiting:
    exit
valid:
    ret
read_ascii_number endp


display proc near ;beginning of procedure
    mov bx, 10 ;initializes divisor
    mov dx, 0000h ;clears dx
    mov cx, 0000h ;clears cx
    ;splitting process starts here
dloop1: mov dx, 0000h ;clears dx during jump
    div bx ;divides ax by bx
    push dx ;pushes dx(remainder) to stack
    inc cx ;increments counter to track the number of digits
    cmp ax, 0 ;checks if there is still something in ax to divide
    jne dloop1 ;jumps if ax is not zero
dloop2:
    pop dx ;pops from stack to dx
    add dx, 30h ;converts to it's ascii equivalent
    mov ah, 02h
    int 21h ;calls dos to display character
    loop dloop2 ;loops till cx equals zero
    ret ;returns control
display endp

dec_to_hex proc near
    mov cx, 0
    mov bx, 16
div1:
    div bx ; divide (wordsized).
    push dx ; save remainder.
    add cx, 1 ; add one to counter
    mov dx, 0 ; clear remainder (dx)
    cmp ax, 0 ; compare quotient (ax) to zero
    jne div1 ; if ax not 0, go to "div1:"
gethex: ; get hex number.
    mov dx, 0 ; clear dx.
    pop dx ; put top of stack into dx.
    add dl, 30h ; conv to character.
    cmp dl, 39h ; if dl > 39h (character 9)...
    jg morehex
hexret: ; display hex number
;mov ah, 02h ; 02h to display dl
    ;int 21h ; send to dos
    loop gethex ; loop subtracts 1 from cx. if nonzero,
                ; loop.
    jmp skip
morehex: ; add 7h if dl > 39h (1015)
    add dl, 7h ; add another 7h to dl to get into the af
               ;hex range.
    jmp hexret ; return to where it left off before adding 7h.
skip: ; skip addition of 7h if it is not needed.
    ret

code ends
end main
