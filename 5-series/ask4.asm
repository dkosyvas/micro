;This program is given as input 16 characters which can only be in range of  1...9 and A...Z and prints them.
;The letters and numbers are seperated in the output with a '-' and the uppercase letters are being converted to lowercase .
;input: A8X9S1FETD73A8KL
;output:891738-axsfetdakl
;When enter is pressed the program ends.
print macro char
    mov dl,char
    mov ah,2
    int 21h
endm

print_str macro string
    mov dx,offset string
    mov ah,9
    int 21h
endm


read macro
    mov ah,08
    int 21h
endm

exit macro
    mov ax,4c00h
    int 21h
endm
      
data_seg segment
    new_line db 0ah,0dh,'$'
    number db 16 dup(0)         ;gia apo8hkeush ari8mwn
    upper_l db 16 dup(0)        ;kefalaiwn xarakthrwn
    input_char db "Input Sequence  : $"    
    out_char   db "Output Secuence : $"
data_seg ends

code_seg segment
    assume cs:code_seg,ds:data_seg
    
main proc far
    mov ax,data_seg
    mov ds,ax
    
start:
    print_str input_char    
    
    mov si,0
clean:
    mov number[si],0            ;arxikopoihsh pinakwn sto 0 
    mov upper_l[si],0
    inc si
    cmp si,16
    jl clean
    
    mov si,0                    ;metriths number
    mov di,0                    ;metriths upper_l
    mov cx,16
    
reader:
    call read_hex               ;diavasma kai ektypwsh 0-9,A-Z, check for enter
    loop reader                 
                                ;loop mexri cx=0   16 characters or if enter pressed exit

after_input:
    print_str new_line
    push di
    mov di,0      

print_str out_char 
print_digits:
    mov al,number[di] ;typwnw ton pinaka me toys ari8moys
    cmp al,0
    je next
    print al
    inc di
    jmp print_digits

next:
    cmp di,0
    je skip_paula               ;an den exoume digits de 8elei paula
    print '-'
skip_paula:
    mov di,0
    
print_lower:                    ;typwnw ton pinaka twn grammatwn afoy kanw thn metatroph prwta 
    mov al,upper_l[di]
    cmp al,0
    je next1 
    add al,32                   ;converting upper case to lower case by adding 32 (ascci code ref)
    print al
    inc di
    jmp print_lower
next1:

    
end1:
    print_str new_line   
    print_str new_line
    jmp start
        
main endp

    
read_hex proc near          ;diabasma kai ektypwsh
    push dx
ignore1:
    read
    cmp al,0Dh             ;termatizetai me enter
    je exit_program

cont1:
    cmp al,30h
    jl ignore1              ;oloi me ascii<'0' agnoountai
    cmp al,39h              ;an ascii>39 den einai ari8mos
    jg addr1
    mov number[si],al       ;apo8hkeush ari8mwn ston pinaka number[16]
    inc si
    jmp addr3
addr1:                      ;den einai ari8mos
    cmp al,'A'
    jl ignore1              ;an ascii<'A' agoeitai
    cmp al,'Z'              
    jg ignore1                
    mov upper_l[di],al      ;apo8hkeush kefalaiwn  ston pinana upper_l[16]
    inc di
    jmp addr3
addr3:
    print al
    pop dx
    ret   
    
exit_program:
    exit
    
read_hex endp

code_seg ends

end main
