;this program prints the hexadecimal, octal and binary format of the decimal
;number given by the user.

;------macros used------------


read macro
    mov ah,8
    int 21h
endm

print macro char
    push ax
    push dx
    mov dl,char
    mov ah,2
    int 21h
    pop dx
    pop ax
endm

print_string macro string
    push ax
    push dx
    mov dx,offset string
    mov ah,9
    int 21h
    pop dx
    pop ax
endm

exit macro
	mov ax,4c00h
	int 21h
endm

new_line macro
	push dx
	push ax
	mov dx,13
	mov ah,2
	int 21h  
	mov dx,10
	mov ah,2
	int 21h
	pop ax
	pop dx
endm
 
;------data segment------------
data_seg segment
    equal db "=$"  
    g1 db "[hex dec octal bin]$"   
 data_seg ends   

;------code segment------------    	   
code_seg segment
    assume cs:code_seg,ds:data_seg
    
    
;----main------------------
main proc far
	mov ax,data_seg
	mov ds,ax 
    print_string g1
    new_line

start:
     
     
     ;first hex digit		
     call hex_keyb
     cmp al,'q'         
     je quit           		;if input is 'q' terminate 
     rol al,4  			;set first digit in msb bits of al 
     mov dl,al   		;put input in dl register
    
     
     ;second hex digit
     call hex_keyb
     cmp al,'q'
     je quit
     add dl,al 			;add units in dl and now dl has the right hex number
     call print_hex
     print_string equal 
     push dx 			;save dl for use in proc "print_oct"
        
     
     call print_dec
     print_string equal
     pop dx 			;restore dl for use in "print_oct"
     push dx			;save dl for use in proc "print_bin" 
     call print_oct
     print_string equal 
     pop dx 			;restore dl for use in "print_bin"
     call print_bin
     
     
     new_line
     jmp start
quit:
    exit     
main endp

;----------procs----------------
hex_keyb proc near
;returns in al resister the ascii code of the pressed key.
;accepts only 0...9 and a...f (hex digits) and character q.  
    push dx
ignore:
	;check for valid number
    read
    cmp al,30h			;if asciicode < 30 ('0') don't accept it
    jl ignore
    cmp al,39h			;if asciicode < 39 ('9') chech for valid character
    jg addr1
    sub al,30h			;if 30 < asciicode < 39 subtract 30 to make hex number
    jmp addr2
addr1: 
	;check for valid character       
    cmp al,'Q'			;if asciicode = 'Q' accept it
    je addr2
    cmp al,'A'			;if asciicode < 'A' don't accept it
    jl ignore
    cmp al,'F'			;if asciicode > 'F' don't accept it
    jg ignore
    sub al,37h			;if 'A' < asciicode < 'F' subtract 37 to make hex number
addr2:
    pop dx
    ret
hex_keyb endp   

print_dec proc near
;input: number in dl 
;output: print the decimal number
    mov ah,0
    mov al,dl 			;ax = 00000000xxxxxxxx(8 bit number to be printed)
    mov bl,10 	
    mov cx,1 			;decades counter
loop_10: 
    div bl			;divide number with 10
    push ax             	;save units     
    cmp al,0 			;if quotient zero i have splitted 
    je print_digits_10  	;the whole number into dec digits     
    inc cx			;increase number of decades
    mov ah,0   
    jmp loop_10			;if quotient is not zero i have to divide again
print_digits_10:
    pop dx			;pop dec digit to be printed
    mov dl,dh
    mov dh,0			;dx = 00000000xxxxxxxx (ascii of number to be printed)
    add dx,30h			;make ascii code
    mov ah,2
    int 21h			;print
    loop print_digits_10	;loop for all digits        
    ret
endp print_dec          

print_oct proc near
;input: number in dl 
;output: print the octal number
    mov ah,0
    mov al,dl 
    mov bl,8 
    mov cx,1 
loop_8: 
    div bl
    push ax                  
    cmp al,0 
    je goout_8              
    inc cx
    mov ah,0   
    jmp loop_8
goout_8: 
    mov dh,al
    push dx   
    
    pop dx
print_digits_8:
    pop dx
    mov dl,dh
    mov dh,0
    add dx,30h
    mov ah,2
    int 21h
    loop print_digits_8       
    ret
endp print_oct       

print_bin proc near
;input: number in dl 
;output: print the binary number
    mov ah,0
    mov al,dl 
    mov bl,2 
    mov cx,1 
loop_2: 
    div bl
    push ax                  
    cmp al,0 
    je goout_2              
    inc cx
    mov ah,0   
    jmp loop_2
goout_2: 
    mov dh,al
    push dx   
    
    pop dx
print_digits_2:
    pop dx
    mov dl,dh
    mov dh,0
    add dx,30h
    mov ah,2
    int 21h
    loop print_digits_2       
    ret
endp print_bin 
         
         
print_hex proc near
;prints hex number 
;first digit
    mov bh,dl 
    and bh,0f0h                 ;isolate first digit's bits (35-->30)
    ror bh,4                    ;rotate 4 times right (30-->3)
    
    cmp bh,9                    
    jg ad1
    add bh,30h                  ;if digit < 9 make ascii by adding 30h (33h)
    jmp ad2
ad1:
    add bh,37h                  ;if digit > 9 make ascii by adding 37h
ad2:
    print bh                    ;print first digit (3: ascii 33)
;second digit   
    mov bh,dl                   
    and bh,0fh                  ;isolate second digit's bits (35-->50)
    cmp bh,9
    jg ad3
    add bh,30h                  ;if digit < 9 make ascii by adding 30h (35h)
    jmp ad4
ad3:
    add bh,37h                  ;if digit > 9 make ascii by adding 37h
ad4:
    print bh                    ;print second digit (5: ascii 35)
    ret   
endp print_hex	

code_seg ends

end main
