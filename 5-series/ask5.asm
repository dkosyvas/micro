;This program displays the temperature as we give the as input voltage from the sensor and the output temperature is calculated with  accuracy of a fractional decimal
;point .In order to calculate the temperature we follow the voltage thresholds given in the diagram and we extract the linear equation for each case .
     ; tmp(V)= 500 * V         , for       V<1    (case1) 
     ; tmp(V)= 500/2 * V +250  , for   1<V<1.8    (case2)
     ; tmp(V)= 1500*V -2000    , for   1.8<V<2    (case3)
;In the input I choose to accept only hexadecimals with lowercase letters so we have.
;some examples of input/outputs for better explanation

;input:2ee   (2ee equals  750  in decimal which corresponds to 0.75 volt )
;output:375.0 

;input:3e8   (3e8 equals  1000  in decimal which corresponds to 1 volt )
;output:500.0 

;input:7ce   (7d0 equals to 1998 in decimal which corresponds to 1.998 volt)  
;output:997.0

;input:7d0   (7d0 equals to 2000 in decimal which corresponds to 2 volt)
;output:ERROR (due to upper temperature threshold of 999.9 Celsius )

PRINT macro message    
    push ax             ;print a message on screen    
    push dx             
    mov dx, offset message
    mov ah, 9
    int 21h
    pop dx
    pop ax
endm
            
data_seg segment
        errormsg db "ERROR"
        newline db 0Dh,0Ah, "$" 
        startornot db "START (Y, N):",0Dh,0Ah, "$"
        input db "insert input: $"
 
data_seg ends

code_seg segment
    assume cs:code_seg,ds:data_seg
    
main proc far
        mov ax,data_seg
        mov ds,ax
            
        PRINT startornot
user_decision:
        mov ah, 1            ;get (y) to start or (n) to terminate
        int 21h              
        cmp al,78            ;this is N
        je end1 
        cmp al,110           ;this is n
        je end1
        cmp al,89            ;this is Y
        je start1 
        cmp al,121           ;this is y
        je start1
        call erasechar
        jmp user_decision
        
       
start1:
        PRINT newline 
        
        

restart:
        mov ch,0 
        mov dx,0
        PRINT input 
        call readhex        ;3 hexadecimal digits=> call read_hex 3 times
        mov bl,al           
        call readhex
        mov bh,al       
        call readhex
        mov cl,al
          
        mov dh,bl           ;place all digits from bl,bh 
        mov ax,10h          ;cl to dx as hexadecimal 
        mul bh
        add dx,ax
        add dx,cx           
        
     ; tmp(V)= 5 * V      , gia V<1  (case1) 
     ; tmp(V)= 5/2 * V +250 , gia  1<V<1.8  (case2)
     ; tmp(V)= 15*V -2000 , gia  1.8<V<2    (case3)
        cmp dx,3E8h         ; V<1
        jl case1
        
        cmp dx,708h         ; v<1.8
        jl case2 
        jmp case3
        
case1:
        mov ax,5
        mul dx              ;input x, output  y=5*x  from 0 to 1 volts
        jmp printres
case2:
        mov ax,5
        mul dx
        mov bx,2
        div bx
        add ax,2500         ;input x, output  y=5/2*x +250  from 1 to 1.8 volts
        jmp printres

case3:
        mov ax,15
        mul dx
        sub ax,20000        ;input x, output  y=5/2*x -2000  from 1.8 to 2 volts
        
printres:
        cmp ax,270Fh        ;temperature 999.9d  is the upper threshold
        jnc error
        PRINT newline       ;integral part is printed
        mov bx,10
        mov dx,0
        div bx
        cmp ax,0
        je zero                              
        call printdex
        jmp decml
        
zero:
        mov ah,0eh
        mov al,'0' 
        int 10h


decml:        
        mov ah,0eh              
        mov al,'.' 
        int 10h
        
        mov ax,dx           ;print decimal part
        cmp ax,0
        je zeronew
        call printdex
        jmp restartnew 
        
zeronew:   
        mov ah,0eh
        mov al,'0' 
        int 10h
                            
restartnew: 
        PRINT newline      ;restart          
        jmp restart
                             
end1:
        hlt
        
        
error:  
        PRINT newline
        PRINT errormsg
        jmp restart 
               
      
 
 
 
erasechar proc near   ;erase the last character pressed
        push ax
        mov ah,0eh
        mov al,8           
        int 10h
        mov al,32           
        int 10h
        mov al,8           
        int 10h
        pop ax 
        ret

      
                  
                  
readhex proc  near     ;read a valid hexadecimal digit

readagain:                 ;if is not valid we read it again
        mov ah, 1               
        int 21h
        cmp al,'n'
        je end1
        cmp al,'N'
        je end1               
        cmp al,48
        jc wronginpt
        cmp al,58
        jnc  wrongnew
        jmp correctinpt
              
wronginpt:
        call erasechar     ;erase the invalid character
        jmp readagain

wrongnewt:
        cmp al,65
        jc wronginpt
        cmp al,103
        jnc wronginpt
        cmp al,71
        jnc wrongnew
        sub al,7
        jmp correctinpt
        
wrongnew:
        cmp al,97
        jc wronginpt
        sub al,39 
        
correctinpt:
        sub al,30h          ;in al have the number pressed in hexadecimal
        ret                 
readhex endp                           
   
        
        
printdex proc near
        push dx             ;show result on screen
        push cx             
        mov dx,0            
        cmp ax,0h           ;the result is decimal because the base is 10
        je end2
        mov cx,10           
        div cx
        push dx
        call printdex
        pop dx
        mov ax,dx
        cmp ax,0ah
        jnc nonumber
        add ax,48
        jmp yesnumber
        
nonumber:
        add ax,55

yesnumber:
        mov ah,0eh      
        int 10h  

end2: 
        pop cx
        pop dx
        ret 

        
code_seg ends

end main
                        
         
